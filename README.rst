Setup environment     
---------
I.e. ::

  source setup.sh

Run BDT training
---------
Compile code, create varlist file ::

  python run.py

...and run with settings defined by the varlist name, e.g.:: 

  nohup python run.py weights/SMT_jvt059_puCut1_tm0_stat4000k_cStat50k_BdtGrad_bin_cuts200_depth6_nTrees300_MNS005_c07.varlist inputFolder/ >& log &
  tail -f log
