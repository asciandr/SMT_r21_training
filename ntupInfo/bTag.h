//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sat Jan 21 11:20:47 2017 by ROOT version 6.04/02
// from TTree bTag_AntiKt4EMTopoJets/bTagAntiKt4EMTopoJets
// found on file: ../../sample_rel21_hybrid/hybrid_SMT.root
//////////////////////////////////////////////////////////

#ifndef bTag_h
#define bTag_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"

using namespace std;

class bTag {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           runnb;
   Int_t           eventnb;
   Int_t           mcchan;
   Double_t        mcwg;
   Int_t           lbn;
   Int_t           coreFlag;
   Int_t           larError;
   Int_t           tileError;
   Int_t           nPV;
   Double_t        PVx;
   Double_t        PVy;
   Double_t        PVz;
   Double_t        avgmu;
   Int_t           actmu;
   Int_t           nbjets_HadF;
   Int_t           nbjets_HadI;
   Int_t           nbjets;
   Int_t           nbjets_q;
   vector<float>   *jet_pt;
   vector<float>   *jet_pt_orig;
   vector<float>   *jet_eta;
   vector<float>   *jet_eta_orig;
   vector<float>   *jet_phi;
   vector<float>   *jet_jf_m;
   vector<float>   *jet_jf_efc;
   vector<float>   *jet_jf_deta;
   vector<float>   *jet_jf_dphi;
   vector<float>   *jet_jf_ntrkAtVx;
   vector<int>     *jet_jf_nvtx;
   vector<float>   *jet_jf_sig3d;
   vector<int>     *jet_jf_nvtx1t;
   vector<int>     *jet_jf_n2t;
   vector<vector<int> > *jet_jf_vtx_ntrk;
   vector<vector<float> > *jet_jf_vtx_L3D;
   vector<vector<float> > *jet_jf_vtx_sig3D;
   vector<float>   *jet_jf_phi;
   vector<float>   *jet_jf_theta;
   vector<vector<float> > *jet_jf_vtx_x;
   vector<vector<float> > *jet_jf_vtx_y;
   vector<vector<float> > *jet_jf_vtx_z;
   Int_t           njets;
   vector<float>   *jet_E;
   vector<float>   *jet_m;
   vector<int>     *jet_truthflav;
   vector<int>     *jet_LabDr_HadF;
   vector<int>     *jet_aliveAfterOR;
   vector<int>     *jet_truthMatch;
   vector<float>   *jet_dRiso;
   vector<float>   *jet_JVT;
   vector<float>   *jet_JVF;
   vector<float>   *jet_ip2d_pb;
   vector<float>   *jet_ip2d_pc;
   vector<float>   *jet_ip2d_pu;
   vector<double>  *jet_ip2d_llr;
   vector<float>   *jet_ip3d_pb;
   vector<float>   *jet_ip3d_pc;
   vector<float>   *jet_ip3d_pu;
   vector<double>  *jet_ip3d_llr;
   vector<float>   *jet_sv1_sig3d;
   vector<int>     *jet_sv1_ntrkj;
   vector<int>     *jet_sv1_ntrkv;
   vector<int>     *jet_sv1_n2t;
   vector<float>   *jet_sv1_m;
   vector<float>   *jet_sv1_efc;
   vector<float>   *jet_sv1_normdist;
   vector<int>     *jet_sv1_Nvtx;
   vector<vector<float> > *jet_sv1_vtx_x;
   vector<vector<float> > *jet_sv1_vtx_y;
   vector<vector<float> > *jet_sv1_vtx_z;
   vector<float>   *jet_sv1_llr;
   vector<float>   *jet_sv1_pu;
   vector<float>   *jet_sv1_pc;
   vector<float>   *jet_sv1_pb;
   vector<double>  *jet_mv2m_pc;
   vector<double>  *jet_mv2m_pu;
   vector<double>  *jet_mv2m_pb;
   vector<double>  *jet_mv2c00;
   vector<double>  *jet_mv2c10;
   vector<double>  *jet_mv2c20;
   vector<double>  *jet_mv2c100;
   vector<float>   *jet_truthPt;
   vector<int>     *jet_GhostL_HadF;
   vector<int>     *jet_GhostL_HadI;
   vector<int>     *jet_GhostL_q;
   vector<int>     *jet_ip3d_ntrk;
   vector<vector<float> > *jet_trk_ip3d_d0sig;
   vector<vector<float> > *jet_trk_ip3d_z0sig;
   vector<vector<int> > *jet_trk_nInnHits;
   vector<vector<int> > *jet_trk_nNextToInnHits;
   vector<vector<int> > *jet_trk_nBLHits;
   vector<vector<int> > *jet_trk_nsharedBLHits;
   vector<vector<int> > *jet_trk_nsplitBLHits;
   vector<vector<int> > *jet_trk_nPixHits;
   vector<vector<int> > *jet_trk_nPixHoles;
   vector<vector<int> > *jet_trk_nsharedPixHits;
   vector<vector<int> > *jet_trk_nsplitPixHits;
   vector<vector<int> > *jet_trk_nSCTHits;
   vector<vector<int> > *jet_trk_nSCTHoles;
   vector<vector<int> > *jet_trk_nsharedSCTHits;
   vector<vector<int> > *jet_trk_expectBLayerHit;
   vector<vector<float> > *jet_trk_pt;
   vector<vector<float> > *jet_trk_eta;
   vector<vector<float> > *jet_trk_theta;
   vector<vector<float> > *jet_trk_phi;
   vector<vector<float> > *jet_trk_qoverp;
   vector<vector<float> > *jet_trk_dr;
   vector<vector<float> > *jet_trk_d0;
   vector<vector<float> > *jet_trk_z0;
   vector<vector<float> > *jet_trk_ip3d_llr;
   vector<vector<int> > *jet_trk_ip3d_grade;
   vector<double>  *jet_mv2c10mu;
   vector<float>   *jet_mu_assJet_pt;
   vector<float>   *jet_mu_truthflav;
   vector<float>   *jet_mu_dR;
   vector<double>  *jet_mu_smt;
   vector<float>   *jet_mu_pTrel;
   vector<float>   *jet_mu_qOverPratio;
   vector<float>   *jet_mu_mombalsignif;
   vector<float>   *jet_mu_scatneighsignif;
   vector<float>   *jet_mu_VtxTyp;
   vector<float>   *jet_mu_pt;
   vector<float>   *jet_mu_eta;
   vector<float>   *jet_mu_phi;
   vector<float>   *jet_mu_d0;
   vector<float>   *jet_mu_z0;
   vector<float>   *jet_mu_parent_pdgid ;
   vector<float>   *jet_mu_ID_qOverP_var;
   vector<float>   *jet_mu_muonType;
   vector<int>     *jet_mu_fatjet_nMu;
   vector<float>   *jet_mu_fatjet_pTmax_pT;
   vector<float>   *jet_mu_fatjet_pTmax_pTrel;
   vector<float>   *jet_mu_fatjet_pTmax_pTrelFrac;

   // List of branches
   TBranch        *b_runnb;   //!
   TBranch        *b_eventnb;   //!
   TBranch        *b_mcchan;   //!
   TBranch        *b_mcwg;   //!
   TBranch        *b_lbn;   //!
   TBranch        *b_coreFlag;   //!
   TBranch        *b_larError;   //!
   TBranch        *b_tileError;   //!
   TBranch        *b_nPV;   //!
   TBranch        *b_PVx;   //!
   TBranch        *b_PVy;   //!
   TBranch        *b_PVz;   //!
   TBranch        *b_avgmu;   //!
   TBranch        *b_actmu;   //!
   TBranch        *b_nbjets_HadF;   //!
   TBranch        *b_nbjets_HadI;   //!
   TBranch        *b_nbjets;   //!
   TBranch        *b_nbjets_q;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_pt_orig;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_eta_orig;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_jf_m;   //!
   TBranch        *b_jet_jf_efc;   //!
   TBranch        *b_jet_jf_deta;   //!
   TBranch        *b_jet_jf_dphi;   //!
   TBranch        *b_jet_jf_ntrkAtVx;   //!
   TBranch        *b_jet_jf_nvtx;   //!
   TBranch        *b_jet_jf_sig3d;   //!
   TBranch        *b_jet_jf_nvtx1t;   //!
   TBranch        *b_jet_jf_n2t;   //!
   TBranch        *b_jet_jf_vtx_ntrk;   //!
   TBranch        *b_jet_jf_vtx_L3D;   //!
   TBranch        *b_jet_jf_vtx_sig3D;   //!
   TBranch        *b_jet_jf_phi;   //!
   TBranch        *b_jet_jf_theta;   //!
   TBranch        *b_jet_jf_vtx_x;   //!
   TBranch        *b_jet_jf_vtx_y;   //!
   TBranch        *b_jet_jf_vtx_z;   //!
   TBranch        *b_njets;   //!
   TBranch        *b_jet_E;   //!
   TBranch        *b_jet_m;   //!
   TBranch        *b_jet_truthflav;   //!
   TBranch        *b_jet_LabDr_HadF;   //!
   TBranch        *b_jet_aliveAfterOR;   //!
   TBranch        *b_jet_truthMatch;   //!
   TBranch        *b_jet_dRiso;   //!
   TBranch        *b_jet_JVT;   //!
   TBranch        *b_jet_JVF;   //!
   TBranch        *b_jet_ip2d_pb;   //!
   TBranch        *b_jet_ip2d_pc;   //!
   TBranch        *b_jet_ip2d_pu;   //!
   TBranch        *b_jet_ip2d_llr;   //!
   TBranch        *b_jet_ip3d_pb;   //!
   TBranch        *b_jet_ip3d_pc;   //!
   TBranch        *b_jet_ip3d_pu;   //!
   TBranch        *b_jet_ip3d_llr;   //!
   TBranch        *b_jet_sv1_sig3d;   //!
   TBranch        *b_jet_sv1_ntrkj;   //!
   TBranch        *b_jet_sv1_ntrkv;   //!
   TBranch        *b_jet_sv1_n2t;   //!
   TBranch        *b_jet_sv1_m;   //!
   TBranch        *b_jet_sv1_efc;   //!
   TBranch        *b_jet_sv1_normdist;   //!
   TBranch        *b_jet_sv1_Nvtx;   //!
   TBranch        *b_jet_sv1_vtx_x;   //!
   TBranch        *b_jet_sv1_vtx_y;   //!
   TBranch        *b_jet_sv1_vtx_z;   //!
   TBranch        *b_jet_sv1_llr;   //!
   TBranch        *b_jet_sv1_pu;   //!
   TBranch        *b_jet_sv1_pc;   //!
   TBranch        *b_jet_sv1_pb;   //!
   TBranch        *b_jet_mv2m_pc;   //!
   TBranch        *b_jet_mv2m_pu;   //!
   TBranch        *b_jet_mv2m_pb;   //!
   TBranch        *b_jet_mv2c00;   //!
   TBranch        *b_jet_mv2c10;   //!
   TBranch        *b_jet_mv2c20;   //!
   TBranch        *b_jet_mv2c100;   //!
   TBranch        *b_jet_truthPt;   //!
   TBranch        *b_jet_GhostL_HadF;   //!
   TBranch        *b_jet_GhostL_HadI;   //!
   TBranch        *b_jet_GhostL_q;   //!
   TBranch        *b_jet_ip3d_ntrk;   //!
   TBranch        *b_jet_trk_ip3d_d0sig;   //!
   TBranch        *b_jet_trk_ip3d_z0sig;   //!
   TBranch        *b_jet_trk_nInnHits;   //!
   TBranch        *b_jet_trk_nNextToInnHits;   //!
   TBranch        *b_jet_trk_nBLHits;   //!
   TBranch        *b_jet_trk_nsharedBLHits;   //!
   TBranch        *b_jet_trk_nsplitBLHits;   //!
   TBranch        *b_jet_trk_nPixHits;   //!
   TBranch        *b_jet_trk_nPixHoles;   //!
   TBranch        *b_jet_trk_nsharedPixHits;   //!
   TBranch        *b_jet_trk_nsplitPixHits;   //!
   TBranch        *b_jet_trk_nSCTHits;   //!
   TBranch        *b_jet_trk_nSCTHoles;   //!
   TBranch        *b_jet_trk_nsharedSCTHits;   //!
   TBranch        *b_jet_trk_expectBLayerHit;   //!
   TBranch        *b_jet_trk_pt;   //!
   TBranch        *b_jet_trk_eta;   //!
   TBranch        *b_jet_trk_theta;   //!
   TBranch        *b_jet_trk_phi;   //!
   TBranch        *b_jet_trk_qoverp;   //!
   TBranch        *b_jet_trk_dr;   //!
   TBranch        *b_jet_trk_d0;   //!
   TBranch        *b_jet_trk_z0;   //!
   TBranch        *b_jet_trk_ip3d_llr;   //!
   TBranch        *b_jet_trk_ip3d_grade;   //!
   TBranch        *b_jet_mv2c10mu;   //!
   TBranch        *b_jet_mu_assJet_pt;   //!
   TBranch        *b_jet_mu_truthflav;   //!
   TBranch        *b_jet_mu_dR;   //!
   TBranch        *b_jet_mu_smt;   //!
   TBranch        *b_jet_mu_pTrel;   //!
   TBranch        *b_jet_mu_qOverPratio;   //!
   TBranch        *b_jet_mu_mombalsignif;   //!
   TBranch        *b_jet_mu_scatneighsignif;   //!
   TBranch        *b_jet_mu_VtxTyp;   //!
   TBranch        *b_jet_mu_pt;   //!
   TBranch        *b_jet_mu_eta;   //!
   TBranch        *b_jet_mu_phi;   //!
   TBranch        *b_jet_mu_d0;   //!
   TBranch        *b_jet_mu_z0;   //!
   TBranch        *b_jet_mu_parent_pdgid ;   //!
   TBranch        *b_jet_mu_ID_qOverP_var;   //!
   TBranch        *b_jet_mu_muonType;   //!
   TBranch        *b_jet_mu_fatjet_nMu;   //!
   TBranch        *b_jet_mu_fatjet_pTmax_pT;   //!
   TBranch        *b_jet_mu_fatjet_pTmax_pTrel;   //!
   TBranch        *b_jet_mu_fatjet_pTmax_pTrelFrac;   //!

   bTag(TTree *tree=0);
   virtual ~bTag();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef bTag_cxx
bTag::bTag(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../../sample_rel21_hybrid/hybrid_SMT.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("../../sample_rel21_hybrid/hybrid_SMT.root");
      }
      f->GetObject("bTag_AntiKt4EMTopoJets",tree);

   }
   Init(tree);
}

bTag::~bTag()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t bTag::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t bTag::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void bTag::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   jet_pt = 0;
   jet_pt_orig = 0;
   jet_eta = 0;
   jet_eta_orig = 0;
   jet_phi = 0;
   jet_jf_m = 0;
   jet_jf_efc = 0;
   jet_jf_deta = 0;
   jet_jf_dphi = 0;
   jet_jf_ntrkAtVx = 0;
   jet_jf_nvtx = 0;
   jet_jf_sig3d = 0;
   jet_jf_nvtx1t = 0;
   jet_jf_n2t = 0;
   jet_jf_vtx_ntrk = 0;
   jet_jf_vtx_L3D = 0;
   jet_jf_vtx_sig3D = 0;
   jet_jf_phi = 0;
   jet_jf_theta = 0;
   jet_jf_vtx_x = 0;
   jet_jf_vtx_y = 0;
   jet_jf_vtx_z = 0;
   jet_E = 0;
   jet_m = 0;
   jet_truthflav = 0;
   jet_LabDr_HadF = 0;
   jet_aliveAfterOR = 0;
   jet_truthMatch = 0;
   jet_dRiso = 0;
   jet_JVT = 0;
   jet_JVF = 0;
   jet_ip2d_pb = 0;
   jet_ip2d_pc = 0;
   jet_ip2d_pu = 0;
   jet_ip2d_llr = 0;
   jet_ip3d_pb = 0;
   jet_ip3d_pc = 0;
   jet_ip3d_pu = 0;
   jet_ip3d_llr = 0;
   jet_sv1_sig3d = 0;
   jet_sv1_ntrkj = 0;
   jet_sv1_ntrkv = 0;
   jet_sv1_n2t = 0;
   jet_sv1_m = 0;
   jet_sv1_efc = 0;
   jet_sv1_normdist = 0;
   jet_sv1_Nvtx = 0;
   jet_sv1_vtx_x = 0;
   jet_sv1_vtx_y = 0;
   jet_sv1_vtx_z = 0;
   jet_sv1_llr = 0;
   jet_sv1_pu = 0;
   jet_sv1_pc = 0;
   jet_sv1_pb = 0;
   jet_mv2m_pc = 0;
   jet_mv2m_pu = 0;
   jet_mv2m_pb = 0;
   jet_mv2c00 = 0;
   jet_mv2c10 = 0;
   jet_mv2c20 = 0;
   jet_mv2c100 = 0;
   jet_truthPt = 0;
   jet_GhostL_HadF = 0;
   jet_GhostL_HadI = 0;
   jet_GhostL_q = 0;
   jet_ip3d_ntrk = 0;
   jet_trk_ip3d_d0sig = 0;
   jet_trk_ip3d_z0sig = 0;
   jet_trk_nInnHits = 0;
   jet_trk_nNextToInnHits = 0;
   jet_trk_nBLHits = 0;
   jet_trk_nsharedBLHits = 0;
   jet_trk_nsplitBLHits = 0;
   jet_trk_nPixHits = 0;
   jet_trk_nPixHoles = 0;
   jet_trk_nsharedPixHits = 0;
   jet_trk_nsplitPixHits = 0;
   jet_trk_nSCTHits = 0;
   jet_trk_nSCTHoles = 0;
   jet_trk_nsharedSCTHits = 0;
   jet_trk_expectBLayerHit = 0;
   jet_trk_pt = 0;
   jet_trk_eta = 0;
   jet_trk_theta = 0;
   jet_trk_phi = 0;
   jet_trk_qoverp = 0;
   jet_trk_dr = 0;
   jet_trk_d0 = 0;
   jet_trk_z0 = 0;
   jet_trk_ip3d_llr = 0;
   jet_trk_ip3d_grade = 0;
   jet_mv2c10mu = 0;
   jet_mu_assJet_pt = 0;
   jet_mu_truthflav = 0;
   jet_mu_dR = 0;
   jet_mu_smt = 0;
   jet_mu_pTrel = 0;
   jet_mu_qOverPratio = 0;
   jet_mu_mombalsignif = 0;
   jet_mu_scatneighsignif = 0;
   jet_mu_VtxTyp = 0;
   jet_mu_pt = 0;
   jet_mu_eta = 0;
   jet_mu_phi = 0;
   jet_mu_d0 = 0;
   jet_mu_z0 = 0;
   jet_mu_parent_pdgid  = 0;
   jet_mu_ID_qOverP_var = 0;
   jet_mu_muonType = 0;
   jet_mu_fatjet_nMu = 0;
   jet_mu_fatjet_pTmax_pT = 0;
   jet_mu_fatjet_pTmax_pTrel = 0;
   jet_mu_fatjet_pTmax_pTrelFrac = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("runnb", &runnb, &b_runnb);
   fChain->SetBranchAddress("eventnb", &eventnb, &b_eventnb);
   fChain->SetBranchAddress("mcchan", &mcchan, &b_mcchan);
   fChain->SetBranchAddress("mcwg", &mcwg, &b_mcwg);
   fChain->SetBranchAddress("lbn", &lbn, &b_lbn);
   fChain->SetBranchAddress("coreFlag", &coreFlag, &b_coreFlag);
   fChain->SetBranchAddress("larError", &larError, &b_larError);
   fChain->SetBranchAddress("tileError", &tileError, &b_tileError);
   fChain->SetBranchAddress("nPV", &nPV, &b_nPV);
   fChain->SetBranchAddress("PVx", &PVx, &b_PVx);
   fChain->SetBranchAddress("PVy", &PVy, &b_PVy);
   fChain->SetBranchAddress("PVz", &PVz, &b_PVz);
   fChain->SetBranchAddress("avgmu", &avgmu, &b_avgmu);
   fChain->SetBranchAddress("actmu", &actmu, &b_actmu);
   fChain->SetBranchAddress("nbjets_HadF", &nbjets_HadF, &b_nbjets_HadF);
   fChain->SetBranchAddress("nbjets_HadI", &nbjets_HadI, &b_nbjets_HadI);
   fChain->SetBranchAddress("nbjets", &nbjets, &b_nbjets);
   fChain->SetBranchAddress("nbjets_q", &nbjets_q, &b_nbjets_q);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_pt_orig", &jet_pt_orig, &b_jet_pt_orig);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_eta_orig", &jet_eta_orig, &b_jet_eta_orig);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_jf_m", &jet_jf_m, &b_jet_jf_m);
   fChain->SetBranchAddress("jet_jf_efc", &jet_jf_efc, &b_jet_jf_efc);
   fChain->SetBranchAddress("jet_jf_deta", &jet_jf_deta, &b_jet_jf_deta);
   fChain->SetBranchAddress("jet_jf_dphi", &jet_jf_dphi, &b_jet_jf_dphi);
   fChain->SetBranchAddress("jet_jf_ntrkAtVx", &jet_jf_ntrkAtVx, &b_jet_jf_ntrkAtVx);
   fChain->SetBranchAddress("jet_jf_nvtx", &jet_jf_nvtx, &b_jet_jf_nvtx);
   fChain->SetBranchAddress("jet_jf_sig3d", &jet_jf_sig3d, &b_jet_jf_sig3d);
   fChain->SetBranchAddress("jet_jf_nvtx1t", &jet_jf_nvtx1t, &b_jet_jf_nvtx1t);
   fChain->SetBranchAddress("jet_jf_n2t", &jet_jf_n2t, &b_jet_jf_n2t);
   fChain->SetBranchAddress("jet_jf_vtx_ntrk", &jet_jf_vtx_ntrk, &b_jet_jf_vtx_ntrk);
   fChain->SetBranchAddress("jet_jf_vtx_L3D", &jet_jf_vtx_L3D, &b_jet_jf_vtx_L3D);
   fChain->SetBranchAddress("jet_jf_vtx_sig3D", &jet_jf_vtx_sig3D, &b_jet_jf_vtx_sig3D);
   fChain->SetBranchAddress("jet_jf_phi", &jet_jf_phi, &b_jet_jf_phi);
   fChain->SetBranchAddress("jet_jf_theta", &jet_jf_theta, &b_jet_jf_theta);
   fChain->SetBranchAddress("jet_jf_vtx_x", &jet_jf_vtx_x, &b_jet_jf_vtx_x);
   fChain->SetBranchAddress("jet_jf_vtx_y", &jet_jf_vtx_y, &b_jet_jf_vtx_y);
   fChain->SetBranchAddress("jet_jf_vtx_z", &jet_jf_vtx_z, &b_jet_jf_vtx_z);
   fChain->SetBranchAddress("njets", &njets, &b_njets);
   fChain->SetBranchAddress("jet_E", &jet_E, &b_jet_E);
   fChain->SetBranchAddress("jet_m", &jet_m, &b_jet_m);
   fChain->SetBranchAddress("jet_truthflav", &jet_truthflav, &b_jet_truthflav);
   fChain->SetBranchAddress("jet_LabDr_HadF", &jet_LabDr_HadF, &b_jet_LabDr_HadF);
   fChain->SetBranchAddress("jet_aliveAfterOR", &jet_aliveAfterOR, &b_jet_aliveAfterOR);
   fChain->SetBranchAddress("jet_truthMatch", &jet_truthMatch, &b_jet_truthMatch);
   fChain->SetBranchAddress("jet_dRiso", &jet_dRiso, &b_jet_dRiso);
   fChain->SetBranchAddress("jet_JVT", &jet_JVT, &b_jet_JVT);
   fChain->SetBranchAddress("jet_JVF", &jet_JVF, &b_jet_JVF);
   fChain->SetBranchAddress("jet_ip2d_pb", &jet_ip2d_pb, &b_jet_ip2d_pb);
   fChain->SetBranchAddress("jet_ip2d_pc", &jet_ip2d_pc, &b_jet_ip2d_pc);
   fChain->SetBranchAddress("jet_ip2d_pu", &jet_ip2d_pu, &b_jet_ip2d_pu);
   fChain->SetBranchAddress("jet_ip2d_llr", &jet_ip2d_llr, &b_jet_ip2d_llr);
   fChain->SetBranchAddress("jet_ip3d_pb", &jet_ip3d_pb, &b_jet_ip3d_pb);
   fChain->SetBranchAddress("jet_ip3d_pc", &jet_ip3d_pc, &b_jet_ip3d_pc);
   fChain->SetBranchAddress("jet_ip3d_pu", &jet_ip3d_pu, &b_jet_ip3d_pu);
   fChain->SetBranchAddress("jet_ip3d_llr", &jet_ip3d_llr, &b_jet_ip3d_llr);
   fChain->SetBranchAddress("jet_sv1_sig3d", &jet_sv1_sig3d, &b_jet_sv1_sig3d);
   fChain->SetBranchAddress("jet_sv1_ntrkj", &jet_sv1_ntrkj, &b_jet_sv1_ntrkj);
   fChain->SetBranchAddress("jet_sv1_ntrkv", &jet_sv1_ntrkv, &b_jet_sv1_ntrkv);
   fChain->SetBranchAddress("jet_sv1_n2t", &jet_sv1_n2t, &b_jet_sv1_n2t);
   fChain->SetBranchAddress("jet_sv1_m", &jet_sv1_m, &b_jet_sv1_m);
   fChain->SetBranchAddress("jet_sv1_efc", &jet_sv1_efc, &b_jet_sv1_efc);
   fChain->SetBranchAddress("jet_sv1_normdist", &jet_sv1_normdist, &b_jet_sv1_normdist);
   fChain->SetBranchAddress("jet_sv1_Nvtx", &jet_sv1_Nvtx, &b_jet_sv1_Nvtx);
   fChain->SetBranchAddress("jet_sv1_vtx_x", &jet_sv1_vtx_x, &b_jet_sv1_vtx_x);
   fChain->SetBranchAddress("jet_sv1_vtx_y", &jet_sv1_vtx_y, &b_jet_sv1_vtx_y);
   fChain->SetBranchAddress("jet_sv1_vtx_z", &jet_sv1_vtx_z, &b_jet_sv1_vtx_z);
   fChain->SetBranchAddress("jet_sv1_llr", &jet_sv1_llr, &b_jet_sv1_llr);
   fChain->SetBranchAddress("jet_sv1_pu", &jet_sv1_pu, &b_jet_sv1_pu);
   fChain->SetBranchAddress("jet_sv1_pc", &jet_sv1_pc, &b_jet_sv1_pc);
   fChain->SetBranchAddress("jet_sv1_pb", &jet_sv1_pb, &b_jet_sv1_pb);
   fChain->SetBranchAddress("jet_mv2m_pc", &jet_mv2m_pc, &b_jet_mv2m_pc);
   fChain->SetBranchAddress("jet_mv2m_pu", &jet_mv2m_pu, &b_jet_mv2m_pu);
   fChain->SetBranchAddress("jet_mv2m_pb", &jet_mv2m_pb, &b_jet_mv2m_pb);
   fChain->SetBranchAddress("jet_mv2c00", &jet_mv2c00, &b_jet_mv2c00);
   fChain->SetBranchAddress("jet_mv2c10", &jet_mv2c10, &b_jet_mv2c10);
   fChain->SetBranchAddress("jet_mv2c20", &jet_mv2c20, &b_jet_mv2c20);
   fChain->SetBranchAddress("jet_mv2c100", &jet_mv2c100, &b_jet_mv2c100);
   fChain->SetBranchAddress("jet_truthPt", &jet_truthPt, &b_jet_truthPt);
   fChain->SetBranchAddress("jet_GhostL_HadF", &jet_GhostL_HadF, &b_jet_GhostL_HadF);
   fChain->SetBranchAddress("jet_GhostL_HadI", &jet_GhostL_HadI, &b_jet_GhostL_HadI);
   fChain->SetBranchAddress("jet_GhostL_q", &jet_GhostL_q, &b_jet_GhostL_q);
   fChain->SetBranchAddress("jet_ip3d_ntrk", &jet_ip3d_ntrk, &b_jet_ip3d_ntrk);
   fChain->SetBranchAddress("jet_trk_ip3d_d0sig", &jet_trk_ip3d_d0sig, &b_jet_trk_ip3d_d0sig);
   fChain->SetBranchAddress("jet_trk_ip3d_z0sig", &jet_trk_ip3d_z0sig, &b_jet_trk_ip3d_z0sig);
   fChain->SetBranchAddress("jet_trk_nInnHits", &jet_trk_nInnHits, &b_jet_trk_nInnHits);
   fChain->SetBranchAddress("jet_trk_nNextToInnHits", &jet_trk_nNextToInnHits, &b_jet_trk_nNextToInnHits);
   fChain->SetBranchAddress("jet_trk_nBLHits", &jet_trk_nBLHits, &b_jet_trk_nBLHits);
   fChain->SetBranchAddress("jet_trk_nsharedBLHits", &jet_trk_nsharedBLHits, &b_jet_trk_nsharedBLHits);
   fChain->SetBranchAddress("jet_trk_nsplitBLHits", &jet_trk_nsplitBLHits, &b_jet_trk_nsplitBLHits);
   fChain->SetBranchAddress("jet_trk_nPixHits", &jet_trk_nPixHits, &b_jet_trk_nPixHits);
   fChain->SetBranchAddress("jet_trk_nPixHoles", &jet_trk_nPixHoles, &b_jet_trk_nPixHoles);
   fChain->SetBranchAddress("jet_trk_nsharedPixHits", &jet_trk_nsharedPixHits, &b_jet_trk_nsharedPixHits);
   fChain->SetBranchAddress("jet_trk_nsplitPixHits", &jet_trk_nsplitPixHits, &b_jet_trk_nsplitPixHits);
   fChain->SetBranchAddress("jet_trk_nSCTHits", &jet_trk_nSCTHits, &b_jet_trk_nSCTHits);
   fChain->SetBranchAddress("jet_trk_nSCTHoles", &jet_trk_nSCTHoles, &b_jet_trk_nSCTHoles);
   fChain->SetBranchAddress("jet_trk_nsharedSCTHits", &jet_trk_nsharedSCTHits, &b_jet_trk_nsharedSCTHits);
   fChain->SetBranchAddress("jet_trk_expectBLayerHit", &jet_trk_expectBLayerHit, &b_jet_trk_expectBLayerHit);
   fChain->SetBranchAddress("jet_trk_pt", &jet_trk_pt, &b_jet_trk_pt);
   fChain->SetBranchAddress("jet_trk_eta", &jet_trk_eta, &b_jet_trk_eta);
   fChain->SetBranchAddress("jet_trk_theta", &jet_trk_theta, &b_jet_trk_theta);
   fChain->SetBranchAddress("jet_trk_phi", &jet_trk_phi, &b_jet_trk_phi);
   fChain->SetBranchAddress("jet_trk_qoverp", &jet_trk_qoverp, &b_jet_trk_qoverp);
   fChain->SetBranchAddress("jet_trk_dr", &jet_trk_dr, &b_jet_trk_dr);
   fChain->SetBranchAddress("jet_trk_d0", &jet_trk_d0, &b_jet_trk_d0);
   fChain->SetBranchAddress("jet_trk_z0", &jet_trk_z0, &b_jet_trk_z0);
   fChain->SetBranchAddress("jet_trk_ip3d_llr", &jet_trk_ip3d_llr, &b_jet_trk_ip3d_llr);
   fChain->SetBranchAddress("jet_trk_ip3d_grade", &jet_trk_ip3d_grade, &b_jet_trk_ip3d_grade);
   fChain->SetBranchAddress("jet_mv2c10mu", &jet_mv2c10mu, &b_jet_mv2c10mu);
   fChain->SetBranchAddress("jet_mu_assJet_pt", &jet_mu_assJet_pt, &b_jet_mu_assJet_pt);
   fChain->SetBranchAddress("jet_mu_truthflav", &jet_mu_truthflav, &b_jet_mu_truthflav);
   fChain->SetBranchAddress("jet_mu_dR", &jet_mu_dR, &b_jet_mu_dR);
   fChain->SetBranchAddress("jet_mu_smt", &jet_mu_smt, &b_jet_mu_smt);
   fChain->SetBranchAddress("jet_mu_pTrel", &jet_mu_pTrel, &b_jet_mu_pTrel);
   fChain->SetBranchAddress("jet_mu_qOverPratio", &jet_mu_qOverPratio, &b_jet_mu_qOverPratio);
   fChain->SetBranchAddress("jet_mu_mombalsignif", &jet_mu_mombalsignif, &b_jet_mu_mombalsignif);
   fChain->SetBranchAddress("jet_mu_scatneighsignif", &jet_mu_scatneighsignif, &b_jet_mu_scatneighsignif);
   fChain->SetBranchAddress("jet_mu_VtxTyp", &jet_mu_VtxTyp, &b_jet_mu_VtxTyp);
   fChain->SetBranchAddress("jet_mu_pt", &jet_mu_pt, &b_jet_mu_pt);
   fChain->SetBranchAddress("jet_mu_eta", &jet_mu_eta, &b_jet_mu_eta);
   fChain->SetBranchAddress("jet_mu_phi", &jet_mu_phi, &b_jet_mu_phi);
   fChain->SetBranchAddress("jet_mu_d0", &jet_mu_d0, &b_jet_mu_d0);
   fChain->SetBranchAddress("jet_mu_z0", &jet_mu_z0, &b_jet_mu_z0);
   fChain->SetBranchAddress("jet_mu_parent_pdgid ", &jet_mu_parent_pdgid , &b_jet_mu_parent_pdgid );
   fChain->SetBranchAddress("jet_mu_ID_qOverP_var", &jet_mu_ID_qOverP_var, &b_jet_mu_ID_qOverP_var);
   fChain->SetBranchAddress("jet_mu_muonType", &jet_mu_muonType, &b_jet_mu_muonType);
   fChain->SetBranchAddress("jet_mu_fatjet_nMu", &jet_mu_fatjet_nMu, &b_jet_mu_fatjet_nMu);
   fChain->SetBranchAddress("jet_mu_fatjet_pTmax_pT", &jet_mu_fatjet_pTmax_pT, &b_jet_mu_fatjet_pTmax_pT);
   fChain->SetBranchAddress("jet_mu_fatjet_pTmax_pTrel", &jet_mu_fatjet_pTmax_pTrel, &b_jet_mu_fatjet_pTmax_pTrel);
   fChain->SetBranchAddress("jet_mu_fatjet_pTmax_pTrelFrac", &jet_mu_fatjet_pTmax_pTrelFrac, &b_jet_mu_fatjet_pTmax_pTrelFrac);
   Notify();
}

Bool_t bTag::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void bTag::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t bTag::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef bTag_cxx
